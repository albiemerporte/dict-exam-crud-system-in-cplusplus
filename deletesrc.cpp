
void Crud::deleterec(const string& nameTodelete){
	
	ifstream ifile("mydb.txt");
	ofstream ofile("temp.txt");
	
	if(!ifile.is_open() || !ofile.is_open()){
		cerr<<"Error Open the File: "<<endl;
		return;
	}
	
	string line;
	bool found = false;
	
	while(getline(ifile, line)){
		if (line.find("Name: " + nameTodelete) != string::npos){
			getline(ifile, line);
			found = true;
			continue;
		}
		ofile<<line<<endl;
	}

	ofile.close();
	ifile.close();
	
	if (!found){
		
		cout<<"record Not Found!"<<endl;
		remove("temp.txt");
	}else{
		remove("mydb.txt");
		rename("temp.txt", "mydb.txt");
		cout<<"Record Deleted Successfully"<<endl;
	}

}
