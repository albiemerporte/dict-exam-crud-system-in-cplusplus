
void Crud::read(string& psh, string& rname, string& rage){     //OK

	ifstream ifile("mydb.txt");
	
	if (!ifile.is_open()){
		cerr<<"Error Open File";
	}
	
	string line;
	bool found = false;
	
	system("cls");
	
	while(getline(ifile, line)){
		if(line.find("Name: " + psh) != string::npos){
			found = true;
			cout<<"FOUND RECORD"<<endl<<endl;
			cout<<"Name: "<<line.substr(6)<<endl;
			rname = line.substr(6);
		}else if(found && line.find("Age:") != string::npos){
			found = false;
			cout<<"Age: "<<line.substr(5)<<endl;
			rage = line.substr(5);
			return;
		}
	}
	
	if(!found){
		cout<<"No Record Found"<<endl;
		rname = "";
		rage = "";
		return;
	}
	
	ifile.close();
	
}
