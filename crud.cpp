
#include <iostream>
#include <fstream>
#include <cstdlib>

#include "crud.h"

using namespace std;

struct Input{
	string fn, ln, age, sh, toupdate, todelete, todeleteconfirm;
	string nfn, nln, nage;
};

Input icreate(){
	Input data;
	
	cout<<"Enter Firstname: ";
	cin>>data.fn;
	cout<<"Enter Lastname: ";
	cin>>data.ln;
	cout<<"Enter Age: ";
	cin>>data.age;
	
	return data;
}

Input iread(){
	Input data;
	
	cout<<"Search: ";
	cin>>data.sh;
	
	return data;
}

Input iupdatesh(){
	Input data;
	cout<<"Search Name to Update: ";
	cin>>data.toupdate;
	
	return data;
}

Input iupdatenew(){
	Input data;
	cout<<"Enter New Firstname: ";
	cin>>data.nfn;
	
	cout<<"Enter New Lastname: ";
	cin>>data.nln;
	
	cout<<"Enter New Age: "<<endl;
	cin>>data.nage;
	
	return data;
}

Input idelete(){
	Input data;
	cout<<"Search Name to Delete: ";
	cin>>data.todelete;
	
	return data;
}

Input deleteconfirm(){
	Input data;
	cout<<"Confirm to Delete [y/n]: ";
	cin>>data.todeleteconfirm;
	
	return data;
}

int main(){
	
	char opt, outnow; //showupdate;
	string name, age;
	
	Crud crud("", "", "");
	
	do{
		cout<<"[C]reate, [R]ead, [U]pdate, [D]elete, [E]xit"<<endl;
		cout<<"Enter Option: ";
		cin>>opt;
	
		if (opt == 'c' or opt == 'C'){
			Input p = icreate();
			Crud crud(p.fn, p.ln, p.age);
			crud.create();
		
		}else if(opt == 'R' or opt == 'r'){
			Input s = iread();
			
			crud.read(s.sh, name, age);
			
		}else if(opt == 'U' or opt == 'u'){
			Input sp = iupdatesh();
			
			crud.read(sp.toupdate, name, age);
			
			if (name != "" && age != ""){
				
				cout<<"Enter New Information for found Record: "<<endl;
				Input iun = iupdatenew();
				crud.update(name, iun.nfn + " " + iun.nln , iun.nage);
			}
			
		}else if(opt == 'D' or opt == 'd'){
    		
    		Input td = idelete();
    		
    		crud.read(td.todelete, name, age);
    		
    		if (name != "" && age != ""){
    			Input tdc = deleteconfirm();
    			crud.deleterec(name);
			}
    		
		}else if(opt == 'e' or opt == 'E'){
			exit(0);
		}
		
		cout<<"\nExit [y/n]: ";
		cin>>outnow;
		system("cls");
	
	}while(outnow != 'y' or outnow != 'y');
	return 0;
}
