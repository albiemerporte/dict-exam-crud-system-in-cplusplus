
void Crud::update(const string& nameToUpdate, const string& newName, const string& newAge){
	
	ifstream ifile("mydb.txt");
	ofstream ofile("temp.txt");
	
	string line;
	bool isNameline = true, found = false;
	
	if (!ifile.is_open() || !ofile.is_open()){
		cerr<<"Unable to Open File"<<endl;
		return;
	}

	while(getline(ifile, line)){
		if(line.find("Name: " + nameToUpdate) != string::npos){
			found = true;
			
			ofile<<"Name: " + newName<<endl;
		
		}else if(found && line.find("Age:") != string::npos){
			found = false;
			
			ofile<<"Age: "<<newAge<<endl;
		}else if(!found){
			ofile<<line<<endl;
		}
	}
	
	ofile.close();
	ifile.close();
	
	remove("mydb.txt");
	rename("temp.txt", "mydb.txt");
	
}
