
#include <cstdlib>

#ifndef CRUD_H
#define CRUD_H

using namespace std;

class Crud{
	private:
		string cfn, cln, cage;
		
	public:
		Crud(const string& fn, const string& ln, const string& age);
		void create();
		void read(string& sh, string& rname, string& rage);
		void update(const string& nameToUpdate, const string& newName, const string& newAge);
		void deleterec(const string& toDelete);
};

Crud::Crud(const string& fn, const string& ln, const string& age) : cfn(fn), cln(ln), cage(age){}

#include "updatesrc.cpp"

#include "createsrc.cpp"

#include "readsrc.cpp"

#include "deletesrc.cpp"

#endif
